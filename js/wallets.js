
const Web3 = "web3";
const Web3Modal = "web3modal";

const providerOptions = {
  
};

const web3Modal = new Web3Modal({
  network: "mainnet", // optional
  cacheProvider: true, // optional
  providerOptions // required
});

const provider = await web3Modal.connectTo("walletconnect");

const web = new Web3(provider); 