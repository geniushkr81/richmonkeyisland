import logo from './assets/img/logo.png';
import './App.css';

//import './assets/js/jquery.min.js';
//import './assets/js/bootstrap.min.js';
//import './assets/js/main.js';
//import './assets/js/popper.js';
import { useWeb3React, Web3ReactProvider } from "@web3-react/core";
import { injected } from './components/wallet/connectors';
import Web3 from 'web3';
import { modal } from './components/modal';


function App() {
  const {active, account, library, connector, activate, deactivate } = useWeb3React();
  async function connect() {
    try {
      await activate(injected)
    } catch (ex){
      console.log(ex)
    }
  }
  async function disconnect(){
    try {
      deactivate()
    } catch (ex) {
      console.log(ex)
    }
  }
  return (
    
    <div className="App">
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
  <div className="container-fluid">
    <a className="navbar-brand" href="#"> <img src={logo} width="50" height="55" alt="" /></a>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>

    <div className="collapse navbar-collapse" id="navbarColor02">
      <ul className="navbar-nav me-auto">
        <li className="nav-item">
          <a className="nav-link active" href="#">Home
          </a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">Features</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">Pricing</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">About</a>
        </li>
       
      </ul>
     
       
     
    </div>
    <button className="navbar active" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <button onClick={connect} className="btn btn-warning btn_wallet" >Connect your Wallet</button>
  </div> 
</nav>
    </div>
  );
}

export default App;
