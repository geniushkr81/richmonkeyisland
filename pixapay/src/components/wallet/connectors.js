import { InjectedConnector } from '@web3-react/injected-connector';
//import { injected } from 'web3modal/dist/providers/connectors';

export const injected = new InjectedConnector({
    supportedChainIds: [1, 3, 4, 5, 42],
})